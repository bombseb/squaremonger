# -*- coding: utf-8 -*-

import os

APP_VERSION = "0.2"
MODULES_PATH = os.path.dirname(os.path.realpath(__file__))
SHARE_PATH = os.path.dirname(MODULES_PATH)
GUI_PATH = os.path.join(SHARE_PATH, 'gui')

class MouseButtons:
    LEFT_BUTTON = 1
    RIGHT_BUTTON = 3

def convertSize(size, r0und=2):
	power = 2**10
	n = 0
	Dic_powerN = {0 : '', 1: 'kb', 2: 'mb', 3: 'gb', 4: 'tb'}
	while size > power:
		size /=  power
		n += 1
	return round (size, r0und), Dic_powerN[n]