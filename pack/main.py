#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk, GLib
import cairo
from Element import Element
import const
import threading

class MainWindow:
	def __init__(self):
		builder = Gtk.Builder()
		builder.add_from_file(os.path.join(const.GUI_PATH, 'mainwindow.glade'))

		for o in builder.get_objects():
			if issubclass(type(o), Gtk.Buildable):
				name = Gtk.Buildable.get_name(o)
				setattr(self, name, o)

		self.drawingArea_1.set_events(Gdk.EventMask.BUTTON_PRESS_MASK | Gdk.EventMask.POINTER_MOTION_MASK) 
		builder.connect_signals (self)

		self.headerbar_1.set_subtitle(const.APP_VERSION)

		self.root = None
		self.scanThread = None
		self.spinner = None
		self.cancelThread = False

		self.window_test.show_all()

		self.motion = False
		self.highlightedElement = None
		a = self.drawingArea_1.get_allocation()
		self.imgSurface = cairo.ImageSurface(cairo.Format.RGB24, a.width, a.height)

	def on_window_test_destroy(self, widget):
		Gtk.main_quit()

	def on_drawingarea_1_draw(self, wid, cr):
		if not self.motion:
			self.redraw()

		self.motion = False
		cr.set_source_surface(self.imgSurface, 0, 0)
		cr.paint()

		if not self.highlightedElement is None:
			self.highlightedElement.highlight(cr, True)

	def on_drawingarea_1_motion_notify(self, wid, e):
		if not self.root is None:
			element = self.root.getElementAtCoords(e)
			if not element is None and element != self.highlightedElement:
				self.motion = True
				self.highlightedElement = element
				self.drawingArea_1.queue_draw()
				size = const.convertSize(element.size)
				self.drawingArea_1.set_tooltip_text(element.path + "\nSize : " + str (size[0]) + size[1])
				# self.drawingArea_1.trigger_tooltip_query()

	def on_drawingArea_1_button_press(self, w, e):
		if e.type == Gdk.EventType.BUTTON_PRESS and e.button == const.MouseButtons.LEFT_BUTTON:
			element = self.root.getElementAtCoords(e)
			if element != None:
				print (element.path, element.size)

	def on_button_openFolder_clicked(self, button):

		# path = "/home/seb/Documents"
		# self.root = Element(path, None)

		# Sans threading
		# self.root = Element(path)
		# self.drawingArea_1.queue_draw()

		response = self.filechooserdialog_folderselect.run()
		self.filechooserdialog_folderselect.hide()
		if response != 1: return
		path = self.filechooserdialog_folderselect.get_filename ()

		self.window_test.remove(self.drawingArea_1)
		self.window_test.add(self.box_wait)

		self.cancelThread = False
		self.scanThread = threading.Thread(target=self.scanFolder, args=(path, lambda: self.cancelThread))
		self.scanThread.daemon = True
		self.scanThread.start()

	def scanFolder(self, *args):
		path,cancel = args
		self.root = Element(path, cancel)
		GLib.idle_add(self.scanCompleted)

	def scanCompleted(self):
		self.window_test.remove(self.box_wait)
		self.window_test.add(self.drawingArea_1)
		self.drawingArea_1.queue_draw()

	def on_button_cancel_clicked(self, button):
		self.cancelThread = True

	def redraw(self):
		if not self.root is None:
			a = self.drawingArea_1.get_allocation()
			self.imgSurface = cairo.ImageSurface(cairo.Format.RGB24, a.width, a.height)
			cr = cairo.Context(self.imgSurface)
			self.root.squarify(0,0,a.width,a.height)
			self.root.draw(cr)

	def on_window_test_check_resize(self, c):
		self.redraw()

def main():
	mainWindow = MainWindow ()
	Gtk.main()
