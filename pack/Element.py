#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import glob, os, sys
import squarify
import cairo
import random

class Element:
    def __init__(self, path, cancelThread, parent=None):
        self.path = path.encode(encoding='UTF-8',errors='replace').decode(sys.getfilesystemencoding())
        self.parent = parent
        self.name = os.path.basename(path)
        self.child = []
        self.size = 0
        self.x = 0
        self.y = 0
        self.width = 0
        self.height = 0
        self.rgb = (0,0,0)
        self.cancelThread = cancelThread
        self.isFile = os.path.isfile(path)

        if not self.isFile:
            self.rgb = (random.random(),random.random(),random.random())

            for f in (glob.glob(glob.escape (self.path) + "/*") +  glob.glob(glob.escape (self.path) + "/.*")):
                if self.cancelThread(): return

                f = f.encode(encoding='UTF-8',errors='replace').decode(sys.getfilesystemencoding())
                if os.path.islink(f): continue

                e = Element(f, self.cancelThread, parent=self)
                if e.size > 0:
                    self.child.append(e)
                    self.size += e.size

            self.child.sort(key = lambda v: v.size, reverse=True)

        elif self.isFile:
            self.size = os.stat(path).st_size
            self.rgb = self.parent.rgb

    def squarify(self, x, y, width, height):
        self.x = x
        self.y = y
        self.width = width
        self.height = height

        if self.isFile:
            return

        if self.width <= 20 or self.height <= 20:
            return

        tmp = [c.size for c in self.child]

        normedSizes = squarify.normalize_sizes(tmp, self.width - 8, self.height - 20)

        rects = squarify.squarify(normedSizes, self.x + 4, self.y + 10, self.width - 8, self.height - 20)
        for c, r in zip (self.child, rects):
            c.squarify(r['x'], r['y'], r['dx'], r['dy'])

    def draw(self, cr):
        padding = 4

        self.x = self.x + padding / 2
        self.y = self.y + padding / 2
        self.width = self.width - padding
        self.height = self.height - padding

        if self.width <= 2 or self.height <= 2:
            return

        self.highlight(cr, False)

        cr.set_source_rgba(*self.rgb, 0.4)
        # cr.set_source_rgb(*self.rgb)
        cr.rectangle(self.x, self.y, self.width, self.height)
        cr.fill()

        cr.rectangle(self.x, self.y, self.width, self.height)
        cr.clip()

        # Labels
        cr.move_to(self.x + 2, self.y + 8)
        cr.set_source_rgb(0,0,0)
        cr.select_font_face("Ubuntu", cairo.FONT_SLANT_NORMAL, cairo.FONT_WEIGHT_NORMAL)
        cr.set_font_size(8)
        
        # (xTxt, yTxt, wTxt, hTxt, dxTxt, dyTxt) = cr.text_extents(self.name)
        # if wTxt < self.width - 2 and hTxt < self.height - 8 :
        cr.show_text(self.name)

        cr.reset_clip()

        # Draw children
        for c in self.child:
            c.draw(cr)

    def highlight(self, cr, highlight=True):
        if highlight:
            h = 1
        else:
            h = 0

        cr.set_line_width(1)
        cr.set_source_rgb(h,0,0)
        cr.rectangle(self.x, self.y, self.width, self.height)
        cr.stroke_preserve()

    def getElementAtCoords(self, coords):
        if coords.x >= self.x and coords.x <= (self.x + self.width) \
            and coords.y >= self.y and coords.y <= (self.y + self.height):

            for c in self.child:
                e = c.getElementAtCoords(coords)
                if not e is None:
                    return e

            return self
        return None

    def getRootElement(self):
        if self.parent is None:
            return self
        else:
            return self.parent.getRootElement()
