#!/bin/sh

INSTALL_DIR="/usr/share/squaremonger"
MAN_DIR="/usr/share/man/man1"
BIN_DIR="/usr/bin"
APP_DIR="/usr/share/applications"
LOC_DIR="/usr/share/locale"
ICO_DIR="/usr/local/share/pixmaps"
VERSION=`cat makedeb.conf | grep "Version" | cut -d\  -f2`

mkdir -p tmp/DEBIAN
cp makedeb.conf tmp/DEBIAN/control

# mkdir -p "tmp"$ICO_DIR
# cp pix/squaremonger.png "tmp"$ICO_DIR

# mkdir -p "tmp"$MAN_DIR
# cp doc/squaremonger.1 "tmp"$MAN_DIR

mkdir -p "tmp"$INSTALL_DIR
cp -R pack "tmp"$INSTALL_DIR
cp -R gui "tmp"$INSTALL_DIR

# mkdir -p "tmp"$INSTALL_DIR"/pix"
# cp pix/*.png "tmp"$INSTALL_DIR"/pix/"

# mkdir -p "tmp"$INSTALL_DIR"/doc"
# cp doc/gpl.txt "tmp"$INSTALL_DIR"/doc/"
# cp doc/whatsnew.txt "tmp"$INSTALL_DIR"/doc/"

mkdir -p "tmp"$APP_DIR
cp res/*.desktop "tmp"$APP_DIR

mkdir -p "tmp"$BIN_DIR
cp squaremonger.py "tmp"$BIN_DIR"/squaremonger"

# mkdir -p "tmp"$LOC_DIR
# cp -R locale/* "tmp"$LOC_DIR

dpkg-deb --build tmp squaremonger-$VERSION.deb
rm -rf tmp