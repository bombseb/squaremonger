#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import os, sys
import builtins

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
if os.path.isfile(os.path.join(SCRIPT_DIR, 'gui', 'mainwindow.glade')) and os.path.isfile(os.path.join(SCRIPT_DIR, 'pack', 'main.py')):
	# not installed
	SHARE_PATH = SCRIPT_DIR
	MODULES_PATH = os.path.join(SCRIPT_DIR, 'pack')
else:
	# installed
	SHARE_PATH = os.path.join(os.path.dirname(SCRIPT_DIR), 'share')
	MODULES_PATH = os.path.join(SHARE_PATH, 'squaremonger', 'pack')
builtins.SHARE_PATH = SHARE_PATH
sys.path.insert(0, MODULES_PATH)

import main
main.main()
