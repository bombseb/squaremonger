# Squaremonger

With Squaremonger you can visualise a treemap of your folders and files, to see what is taking so much place on your harddrive.
It is inspired by the great Spacemonger app for windows, but this one is for Linux and made with Python.

Squaremonger is using the [Squarify](https://github.com/laserson/squarify) library for python

# Installation

## PPA
Coming soon...

# Usage
Just launch the application, click the folder icon at the top left of the main window and select a directory to scan. Squaremonger show a treemap representing all the files and subdirectories.

# Contributing
1.  Fork it!
2.  Create your feature branch:  `git checkout -b my-new-feature`
3.  Commit your changes:  `git commit -am 'Add some feature'`
4.  Push to the branch:  `git push origin my-new-feature`
5.  Submit a pull request :D

# License

Coming soon...
